/*
	Paul Lawrence Z Sales
	2015-01214
	Machine Problem 12 - Infix Calc
	This MP was made possible by the following:
		CS32 class discussions (Thank you Sir Edgar! <3)
		Data Structures by Evangel P. Quiwa
	25 October 2017
 */
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>


typedef struct node STACKNODE;
typedef float STACK_ELEMTYPE;

//error handling
void StackOverflow() {
	printf("Stackoverflow detected\n");
	exit(1);
}
void StackUnderflow() {
	printf("Stackunderflow detected\n");
	exit(1);
}

struct node
{
	STACK_ELEMTYPE value;
	STACKNODE *next;
};

struct stack
{
	STACKNODE *top;
};
typedef struct stack STACK;

void initStack(STACK *S) {
	S->top = NULL;
}

void PUSH(STACK *S, STACK_ELEMTYPE x) {
	//create a new node for the incoming value
	STACKNODE *newNode = (STACKNODE*)malloc(sizeof(STACKNODE));
	if(newNode == NULL) { //can't allocate memory anymore
		StackOverflow();
	}
	newNode->value = x;

	//change the top of stack
	newNode->next = S->top; //point newNode to where S->top is pointing
	S->top = newNode; //point S->top to newNode. newNode is now top of the stack
}

void POP(STACK *S, STACKNODE *poppedNode) {
	if(S == NULL) { //stack S is empty
		StackUnderflow();
	}
	//Create a tempNode for the node to be popped
	STACKNODE *tempNode = (STACKNODE*)malloc(sizeof(STACKNODE));
	tempNode = S->top; //assign the current top to tempNode

	//Change the top by pointing S->top to the next node
	S->top = S->top->next;
	//Get the value of the previous top
	poppedNode->value = tempNode->value;
	free(tempNode); //free the mem of tempNode
}

int PEEK(STACK *S) {
	return S->top->value;
}

int isp(char operator) {
	if(operator == '+' || operator == '-')
		return 2;
	if(operator == '*' || operator == '/')
		return 4;
	if(operator == '^')
		return 5;
	if(operator == '(')
		return -1;

	return 0; //erorr
}

int icp(char operator) {
	if(operator == '+' || operator == '-')
		return 1;
	if(operator == '*' || operator == '/')
		return 3;
	if(operator == '^')
		return 6;

	return 0; //error
}

int main(int argc, char const *argv[])
{
	/* code */
	char *postfixString = (char*)malloc(sizeof (char)*100);
	int iterInput = 0, iterPFS = 0;
	long double result;
	int holder;
	STACKNODE *tempNode;

	STACK *S = (STACK*)malloc(sizeof(STACK));
	initStack(S);
	STACK *R = (STACK*)malloc(sizeof(STACK));
	initStack(R);

	while(1) {
		holder = getchar(); //get the characters of the input

		

		if(isdigit(holder)) {
			printf("is digit\n");
			break;
			*(postfixString + iterPFS) = holder;
			iterPFS += 1;
			iterInput += 1;
			continue;
		}

		if(holder == ' ') {
			iterInput += 1;
			continue;
		}

		if(holder == '('){
			PUSH(S, holder);
			iterInput += 1;
			continue;
		}

		if(holder == ')') {
			while(PEEK(S) != '(') {
				tempNode = (STACKNODE*)malloc(sizeof(STACKNODE));
				POP(S, tempNode);
				*(postfixString + iterPFS) = tempNode->value;
				iterPFS += 1;
				free(tempNode);
			}
			tempNode = (STACKNODE*)malloc(sizeof(STACKNODE)); //pop '('
			POP(S, tempNode);
			free(tempNode);

			if(S->top == NULL) {
				// printf("break free\n");
				break;
			}
			iterInput += 1;
			continue;
		}

		//else
		while(icp(holder) < isp(PEEK(S))) {
			tempNode = (STACKNODE*)malloc(sizeof(STACKNODE));
			POP(S, tempNode);
			*(postfixString + iterPFS) = tempNode->value;
			iterPFS += 1;
		}

		if(icp(holder) > isp(PEEK(S))) {
			PUSH(S, holder);
			iterInput += 1;
		}

	}
	printf("%s\n", postfixString );
	return 0;
}