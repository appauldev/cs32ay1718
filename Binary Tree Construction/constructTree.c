/*
	Paul Lawrence Z Sales
	2015-01214
	Machine Problem 4 - Binary Tree Construction from Traversal Outputs
	This MP was made possible by the following:
		CS32 class discussions (Thank you Sir Edgar! <3)
		Data Structures by Evangel P. Quiwa
	15 October 2017
 */

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

//create a static  variable for the function
//this variable will keep track of the iteration in the preString
static int ITER = 0;

//linked list and queue
struct node {
	char value;
	struct node *llink;
	struct node *rlink;
};

struct node *createNode(char input) {
	struct node *new = (struct node*)malloc(sizeof(struct node));
	new->value = input;
	new->llink = NULL;
	new->rlink = NULL;

	return new;
}

struct node *initTree(char input) {
	struct node *new = (struct node*)malloc(sizeof(struct node));
	new->value = input;
	new->llink = NULL;
	new->rlink = NULL;

	return new;
}

void postOrder(struct node *root) {
	if (root == NULL) {
		return;
	}
	else {
		postOrder(root->llink);
		postOrder(root->rlink);
		printf("%c", root->value);
	}
}

struct node* contructTree(char *preString, char *inString, int leftBound, int rightBound) {

	if(leftBound > rightBound) {
		return NULL;
	}

	struct node* newNode = createNode(*(preString + ITER));
	//iterate the global variable
	ITER += 1;

	if(leftBound == rightBound) {
		return newNode;
	}

	//search for the index of the current node value in the inorder string
	int inIdx = 0;
	for(inIdx = leftBound; inIdx <= rightBound; inIdx++) {
		if(newNode->value == *(inString+inIdx)) {
			break;
		}
	}

	newNode->llink = contructTree(preString, inString, leftBound, inIdx-1);
	newNode->rlink = contructTree(preString, inString, inIdx+1, rightBound);

	return newNode;
}
int main(int argc, char const *argv[])
{
	/* code */
	int testCases;

	//get the number of testCases
	scanf("%i", &testCases);

	while(testCases != 0) {
		//make and initialize variables for the traversal strings
		char *preString, *inString;
		preString = (char*)malloc(sizeof(preString)*52);
		inString = (char*)malloc(sizeof(inString)*52);

		//get input from the stdout and store to respective string container i.e. preString and inString
		fscanf(stdin, "%s %s", preString, inString);
		// printf("%s %s\n", preString, inString);
		
		//get the length of strings;
		int preLen = strlen(preString);
		// inLen = strlen(inString);
		int leftBound = 0;
		int rightBound = preLen-1;

		//initialize the tree
		struct node *root = contructTree(preString, inString, leftBound, rightBound);
		// printf("%c\n", root->value);
		postOrder(root);
		printf("\n");

		testCases -= 1;
		ITER = 0; //reset the global variable for next call
		//free the allocations
		free(root);
		free(preString);
		free(inString);
	}


	return 0;
}

	// struct node *root = initTree('A');
	// root->llink = createNode('L');
	// printf("here\n");
	// root->llink->llink = createNode('G');
	// root->llink->llink->llink = createNode('O');
	// root->llink->rlink = createNode('I');
	// root->llink->llink->rlink = createNode('R');
	// root->rlink = createNode('T');
	// root->rlink->llink = createNode('H');
	// root->rlink->llink->rlink = createNode('M');
	// root->rlink->rlink = createNode('S');

	// postOrder(root);