/*
	Paul Lawrence Z Sales
	2015-01214
	Machine Problem 2 - Balanced Parentheses
	This MP was made possible by the following:
		CS32 class discussions (Thank you Sir Edgar! <3)
		Data Structures by Evangel P. Quiwa
	15 October 2017
 */
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

void StackOverflow() {
	printf("Stackoverflow detected\n");
	exit(1);
}

void StackUnderflow() {
	printf("Stackunderflow detected\n");
	exit(1);
}

struct node {
	char data;
	struct node* nextNode;
};

//Stack
struct stack {
	struct node* top;
};

void initStack(struct stack* S) {
	S->top = NULL;
}

void PUSH(struct stack *S, char streamData) {
	//create a new node and check if the node is created. If not, call stack overflow
	struct node* newNode = (struct node*)malloc(sizeof(struct node));
	if(newNode == NULL) {
		StackOverflow();
	}
	//node is created, assign the data
	newNode->data = streamData;
	//point the nextNode of the newNode to the current top of Stack
	newNode->nextNode = S->top;
	//point the top of the stack to the newly created node
	S->top = newNode;
}

void POP(struct stack* S, struct node* poppedNode) {
	struct node* tempNode = (struct node*)malloc(sizeof(struct node));
	if(S->top == NULL) {
		StackUnderflow();
	}

	//create a temp node for the current top
	tempNode = S->top;
	//point the top of the stack to the next of the current top
	S->top = S->top->nextNode;
	//get the data of the popped node and free the mem
	poppedNode->data = tempNode->data;
	free(tempNode);

}

int isLeft(char data) {
	if(data == '(' || data == '{' || data == '[') {
		return 1;
	}
	return 0;
}

int isRight(char data) {
	if(data == ')' || data == '}' || data == ']') {
		return 1;
	}
	return 0;
}

int isPair(char left, char right) {
	if(left == '(' && right == ')') {
		return 1;
	}
	if(left == '[' && right == ']') {
		return 1;
	}
	if(left == '{' && right == '}') {
		return 1;
	}
	return 0;
}

int isNotSpace(char n) {
	if(n != ' ') {
		// printf("isnotspace is %c\n", n);
		return 1;
	}
	return 0;
}

int main(int argc, char const *argv[])
{
	char n;
	int count = 0, endLoop = 1, keepLoop = 1;
	struct stack *S;
	

	while(endLoop) {
		//keep track of the case count
		count += 1;
		S = (struct stack*)malloc(sizeof(struct stack));
		initStack(S);
		//check if end of input is found
		// while((m = getchar()) == '*') { //means ***end*** is the current input
		// 	printf("m is %c\n", m);
		// 	endLoop = 0;
		// 	break;			
		// }
		
		//	%*[^ ] ignores every incoming character from the stdin 
		//		until we find a space (space is not stored)
		//	ignores "CASE "
		//	%*[^:] ignore every incoming character after 
		//the last read until we find a : (: is not stored)
		//	ignores "1:"
		//	%*[^ ] ignores " "
		//fscanf terminates reading if it encounters a space or \0
		fscanf(stdin, "%*[^ ]%*[^:]%*[^ ]"); //strips "CASE N:" the space character (" ") is still in the stdin

		//next character must be a space. 
		if(isNotSpace(getchar())) { //If it is not a space, it means end of input is reached
			free(S); //free the previous mem alloc
			break; //terminate the program
		}

		while(keepLoop) {
			n = getchar();
			// printf("n is %c\n", n);
			
			if(n == '\n') { //end of string is reached, check if the parentheses are balanced
				if(S->top != NULL) { //stack still has an element, not balanced
					printf("CASE %i: NOT BALANCED\n", count);
					break; //next case
				} 

				//if not null, it means the case is balanced
				printf("CASE %i: BALANCED\n", count);
				break;
			}

			//Check the incoming input. Only accept left parentheses for initial input of the stack
			if(isLeft(n)) { //push n if it is a left parenthesis
				PUSH(S, n);
				continue;
				// printf("%c\n", n);
			}
			//if input is a right parentheses, check if it is a pair of the top of the stack
			if(isRight(n)) {
				if(S->top == NULL) { //stack is empty, no left pair for the current input
					printf("CASE %i: NOT BALANCED\n", count);
					break; //next case
				}
				//stack is not empty, check if the top is a pair of the input
				struct node *poppedNode = (struct node*)malloc(sizeof(struct node));
				POP(S, poppedNode); //get the top
				if(isPair(poppedNode->data, n)) { //determine if it is a pair
					//if it is a pair, free the recently created node and continue to the
					// next input of the current case
					free(poppedNode);
					continue;
				}
				else { //input is not a pair of the top of the stack
					printf("CASE %i: NOT BALANCED\n", count);
					break; //next case
				}
			}
		}
		//free mem alloc for S and recreate S for next case
		free(S);

	}
	return 0;
}

