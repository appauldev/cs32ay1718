/*
	Paul Lawrence Z Sales
	2015-01214
	Machine Problem 3 - Topological Sorting
	This MP was made possible by the following:
		CS32 class discussions (Thank you Sir Edgar! <3)
		Data Structures by Evangel P. Quiwa
	22 September 2017
 */

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

//For output formatting
char* concat(char *str1, char *str2) {
    char *result = malloc(sizeof(char)*500);
	strcat(str1, "\0"); //valgrind debug
	strcat(str2, "\0"); //valgrind debug
    strcpy(result, str1);
    strcat(result, str2);
    return result;
}

//linked list and queue
struct node {
	int value;
	struct node *next_node;
};

struct queue {
	struct node *front;
	struct node *rear;
};
typedef struct queue Queue;

//for debugging
void printSuccessors (struct node *head) {
	struct node *tempNode = (struct node *)malloc(sizeof(struct node));
	tempNode = head;
	while (tempNode != NULL)
	{
		printf("%i ", tempNode->value);
		tempNode = tempNode->next_node;
	}
	free(tempNode);
}

//Main Function
int main(int argc, char const *argv[]) {
	//Main variables
	int testCases,numOfElem, numOfPrece;
	int i,j;
	char *tuple = malloc(sizeof(char)*100);

	//get the number of testcases
	scanf("%i", &testCases);
	// printf("number of test case is %i\n", testCases );

	while(testCases != 0)
	{
		//get the number of elements to be sorted
		scanf("%i", &numOfElem);
		numOfElem += 1;
		// printf("number of elements is %i\n", numOfElem );

		//make the COUNT array based from the number of elements
		int COUNT[numOfElem];
		//initialize COUNT with 0s
		int n, t; //iterators
		for(n = 0; n < numOfElem; n++) {
			*(COUNT + n) = 0;
		}
		
		//initialize the linked-list for the successor
		//set the values to NULL
		struct node *LIST[numOfElem];
		for(n = 0; n < numOfElem; n++) {
			LIST[n] = NULL;
		}

		//number of precedences
		scanf("%i", &numOfPrece);
		// printf("number of precedences is %i\n", numOfPrece );

		//process the precedences
		for (t = 0; t < numOfPrece; t++) {
			//scanning the (i,j) i precedes j
			scanf("%s", tuple);
			// printf("%s\n", tuple );	
			sscanf(tuple, "(%i,%i)", &i, &j);

			//update the COUNT of predecessors of j
			*(COUNT + j) += 1;
			// printf("COUNT of predecessors of %i is %i\n", j, *(COUNT + j));

			//add j to the list of successors of i
			struct node *newSuccessor = (struct node *)malloc(sizeof(struct node));
			newSuccessor->value = j;
			newSuccessor->next_node = LIST[i];
			LIST[i] = newSuccessor;
			// printSuccessors(LIST[i]);
			// printf("\n");
		}
		//prevent memory leaks
		// free(tuple);

		//Initialize the output QLINK
		int *QLINK = COUNT;
		int front = 0, rear;
		for (n = 1; n < numOfElem; n++) {
			if (COUNT[n] == 0) {
				if (front == 0) {
					rear = n;
					front = rear;
				}
				else {
					*(QLINK + rear) = n;
					rear = n;
				}
			}
		}

		//initialize the output string
		char *output = malloc(sizeof(char)*500);
		char *strInt = malloc(sizeof(char)*100);

		//Output the partial ordering
		while(front != 0) {
			// printf("%i\n", front);
			sprintf(strInt, "%i", front);
			//concatenate
			output = concat(output, strInt);

			struct node *tempNode = (struct node *)malloc(sizeof(struct node));
			tempNode = LIST[front];
			while (tempNode != NULL) {
				int tempSucc = tempNode->value;
				COUNT[tempSucc] -= 1;
				if (COUNT[tempSucc] == 0) {
					*(QLINK + rear) = tempSucc;
					rear = tempSucc;
				}
				tempNode = tempNode->next_node;
			}
			front = *(QLINK + front);

			//Formatting the output string
			if (front != 0) {
				//add comma and space
				output = concat(output, ", ");
			}
			else {
				//if front is the last item, add terminator to the string
				output = concat(output, "\0");
			}
		}
		printf("%s\n",output );

		//Go to the next testCase
		testCases -= 1;
	}	
	return 0;
}